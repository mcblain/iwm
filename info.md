### DWM
variables:
  cursor:
    Массив указателей на Cur.
    Отдельный элемент отвечает за одно состояние курсора
    Их число фиксировано.

Button:
  click - цель нажатия мышью
  mask - дополнительные кнопки (напр. Mod1)
  button - номер кнопки мыши
  func - функция вызываемая при нажатии
  arg - аргумент функции

Arg:
  i - int
  ui - unsigned int
  f - float
  v - void pointer

Client:
  name - строка с названием окна.
         Имеет смысл вынести в отдельную структуру
  mina/maxa - минимальный и максимальный aspect ratio
  x, y, w, h - ясно
  oldx, oldy, oldw, oldh - используется для восстановления после выхода с фуллскрина
  basew, baseh, incw, inch, maxw, minh, hintsvalid - size hints
  bw, oldbw - border width
  tags - 

  




### DRW
Cur:
  Курсор мыши
  Хранятся в фиксированном массиве

Fnt:
  dpy - указатель на Display
  h - высота шрифта
  xfont - указатель на данные шрифта
  pattern - хранит некий паттерн. Он так же есть и в xfont но этот круче

Clr:
  Алиас к XftColor

Drw:
  w - ширина монитора
  h - высора монитора
  dpy - указатель на Display
  screen - id экрана дисплея (Display)
  root - id корневого окна
  drawable - id рисователя
  gc - указатель на Графический Контекст

-----------

Monitor {
  clients: Vec<Client>,
  order: Vec<u8>,
  stack: Vec<u8>,
  visible: Vec<u8>,
  selected: usize,
}

-- Перемещение в стеке
-- Перемещение в очереди
-- Перемещение между тегами
-- Перемещение между мониторами












