#![allow(dead_code)]

mod client;
mod monitor;

pub mod drw;
pub mod util;
pub mod x;

use client::Client;
use monitor::Monitor;
use std::{mem, ptr};
use util::{cstr, struct_len};
use x::{xinerama, XK};

pub use drw::{Colorscheme, ColorschemeModes};

pub type WMResult<T> = Result<T, String>;

pub struct Layout {
    pub symbol: &'static str,
    pub func: fn(&mut WM),
}

impl std::fmt::Debug for Layout {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.symbol)
    }
}

#[derive(Debug)]
pub enum Arg {
    Int(isize),
    Cmd(Vec<&'static str>),
    Layout(Layout),
}

pub struct Key {
    pub mod_key: u32,
    pub key_sym: u32,
    pub func: fn(arg: &Arg, wm: &mut WM),
    pub arg: Arg,
}

impl std::fmt::Debug for Key {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "key: {}, sym: {}", self.mod_key, self.key_sym)
    }
}

#[derive(PartialEq, Eq, Debug)]
pub enum Click {
    TagBar,
    LtSymbol,
    StatusText,
    WinTitle,
    ClientWin,
    RootWin,
    Last,
}

pub struct Button {
    pub click: Click,
    pub mask: u32,
    pub func: fn(arg: &mut Arg, wm: &mut WM),
    pub button: u32,
    pub arg: Arg,
}

impl std::fmt::Debug for Button {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Click: {:?}", self.click)
    }
}

#[derive(Debug)]
pub struct Config {
    pub border_width: u8,
    pub snap: u8,
    pub show_bar: bool,
    pub top_bar: bool,
    pub fonts: Vec<&'static str>,
    pub tags: Vec<&'static str>,
    pub master_area_factor: f32,
    pub master_cnt: u8,
    pub resize_hints: bool,
    pub lock_fullscreen: bool,
    pub layouts: Vec<Layout>,
    pub keys: Vec<Key>,
    pub buttons: Vec<Button>,
    pub colorscheme: ColorschemeModes<&'static str>,
}

static mut XERROR_XLIB: Option<x::ErrorHandler> = None;

pub fn supports_locale() -> bool {
    unsafe { !libc::setlocale(libc::LC_CTYPE, cstr("").as_ptr()).is_null() && x::supports_locale() }
}

pub fn dummy_arrange(_: &mut WM) {}

pub fn dummy_btn_func(_: &mut Arg, _: &mut WM) {}

pub fn spawn(_: &Arg, _: &mut WM) {
    // unsafe {
    //     if libc::fork() == 0 {
    //         libc::close(xlib::XConnectionNumber(mut_dpy!(wm.drw.dpy)));

    //         if let Arg::Cmd(cmd) = arg {
    //             process::Command::new(cmd.0).args(cmd.1);
    //         }
    //     }
    // }
}

// use crate::drw::Color;
// use client::Client;
// use drw::{Colorscheme, DisplayRc};
// use std::{
//     fmt::Debug,
//     mem,
//     ops::DerefMut,
//     process::{self, exit},
//     ptr,
// };
// use util::cstr;
// use workspace::Workspace;
// use x::{Display, DisplayRaw};
// use x11::{
//     keysym, xinerama,
//     xlib::{self, False, True},
// };

// pub fn dummy_arrange(_: &mut Workspace) {}

// const BUTTOM_MASK: u32 = (xlib::ButtonPressMask | xlib::ButtonReleaseMask) as u32;

unsafe extern "C" fn xerror(dpy: *mut x::DisplayRaw, ee: *mut x::ErrorEvent) -> i32 {
    let request_code = (*ee).request_code;
    let error_code = (*ee).error_code;
    if matches!(
        (request_code, error_code),
        (_, x::BAD_WINDOW)
            | (42, x::BAD_MATCH)
            | (74, x::BAD_DRAWABLE)
            | (70, x::BAD_DRAWABLE)
            | (66, x::BAD_DRAWABLE)
            | (12, x::BAD_MATCH)
            | (28, x::BAD_ACCESS)
            | (33, x::BAD_ACCESS)
            | (62, x::BAD_DRAWABLE)
    ) {
        0
    } else {
        err!("fatal error: request code={request_code}, error code={error_code}");
        XERROR_XLIB.map(|func| func(dpy, ee)).unwrap_or_default()
    }
}

fn checkotherwm(dpy: &mut x::Display) {
    unsafe extern "C" fn xerrorstart(_: *mut x::DisplayRaw, _: *mut x::ErrorEvent) -> i32 {
        err!("another window manager is already running");
        std::process::exit(1);
    }

    unsafe {
        XERROR_XLIB = x::set_error_handler(xerrorstart);
        let root_window = dpy.default_root_window();
        dpy.select_input(root_window, x::SUBSTRUCTURE_REDIRECT_MASK);
        dpy.sync(false);
        x::set_error_handler(xerror);
        dpy.sync(false);
    }
}

// // structs

// const WM_ATOMS_LEN: usize = 4;
// enum WMAtom {
//     Protocols,
//     Delete,
//     State,
//     TakeFocus,
//     Last,
// }
// const NET_ATOMS_LEN: usize = 9;
// enum NetAtom {
//     Supported,
//     WMName,
//     WMState,
//     WMCheck,
//     WMFullscreen,
//     ActiveWindow,
//     WMWindowType,
//     WMWindowTypeDialog,
//     ClientList,
// }

// impl Debug for Layout {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         write!(f, "{}", self.symbol)
//     }
// }

// #[derive(Debug)]
// struct CursorModes {
//     pub normal: drw::Cursor,
//     pub resize: drw::Cursor,
//     pub drag: drw::Cursor,
// }

#[derive(Debug)]
struct WMAtoms {
    protocols: x::Atom,
    delete: x::Atom,
    state: x::Atom,
    take_focus: x::Atom,
}

#[derive(Debug)]
struct NetAtoms {
    active_window: x::Atom,
    supported: x::Atom,
    wm_name: x::Atom,
    wm_state: x::Atom,
    wm_check: x::Atom,
    wm_fullscreen: x::Atom,
    wm_window_type: x::Atom,
    wm_window_type_dialog: x::Atom,
    clients_list: x::Atom,
}

#[derive(Debug)]
struct Cursors {
    normal: drw::Cursor,
    resize: drw::Cursor,
    drag: drw::Cursor,
}

pub struct Monitors {
    mons: Vec<Monitor>,
}

#[derive(Debug)]
pub struct WM {
    colorscheme: ColorschemeModes<drw::Color>,
    config: Config,
    cursors: Cursors,
    net_atoms: NetAtoms,
    num_lock_mask: u32,
    pub bar_height: i16,
    pub drw: drw::Drw,
    pub lrpad: i16,
    pub mons: Vec<Monitor>,
    pub root: x::Window,
    pub selmon: usize,
    wm_atoms: WMAtoms,
    wm_check_win: x::Window,
}

impl WM {
    pub fn new(config: Config) -> WMResult<Self> {
        let mut dpy = match x::Display::new() {
            Some(dpy) => dpy,
            None => return Err("cannot open display".into()),
        };

        checkotherwm(&mut dpy);

        sigchld();

        let screen = dpy.default_screen();
        let mut drw = drw::Drw::new(dpy, screen);

        drw.fonts = drw.create_fontset(&config.fonts)?;

        let lrpad = drw.fonts[0].h;
        let bar_height = drw.fonts[0].h + 2;

        let mut dpy_handle = drw.dpy.borrow_mut();

        let utf8_string = x::intern_atom(&mut dpy_handle, "UTF8_STRING", false);

        let wm_atoms = WMAtoms {
            protocols: x::intern_atom(&mut dpy_handle, "WM_PROTOCOLS", false),
            delete: x::intern_atom(&mut dpy_handle, "WM_DELETE_WINDOW", false),
            state: x::intern_atom(&mut dpy_handle, "WM_STATE", false),
            take_focus: x::intern_atom(&mut dpy_handle, "WM_TAKE_FOCUS", false),
        };

        let net_atoms = NetAtoms {
            active_window: x::intern_atom(&mut dpy_handle, "_NET_ACTIVE_WINDOW", false),
            supported: x::intern_atom(&mut dpy_handle, "_NET_SUPPORTED", false),
            wm_name: x::intern_atom(&mut dpy_handle, "_NET_WM_NAME", false),
            wm_state: x::intern_atom(&mut dpy_handle, "_NET_WM_STATE", false),
            wm_check: x::intern_atom(&mut dpy_handle, "_NET_SUPPORTING_WM_CHECK", false),
            wm_fullscreen: x::intern_atom(&mut dpy_handle, "_NET_WM_STATE_FULLSCREEN", false),
            wm_window_type: x::intern_atom(&mut dpy_handle, "_NET_WM_WINDOW_TYPE", false),
            wm_window_type_dialog: x::intern_atom(
                &mut dpy_handle,
                "_NET_WM_WINDOW_TYPE_DIALOG",
                false,
            ),
            clients_list: x::intern_atom(&mut dpy_handle, "_NET_CLIENT_LIST", false),
        };

        drop(dpy_handle);

        // init cursors
        let cursors = Cursors {
            normal: drw::Cursor::new(drw.dpy.clone(), 68),
            resize: drw::Cursor::new(drw.dpy.clone(), 120),
            drag: drw::Cursor::new(drw.dpy.clone(), 52),
        };

        let mut dpy_handle = drw.dpy.borrow_mut();

        // init appearance
        let colorscheme = ColorschemeModes::new(&config.colorscheme, screen, &mut dpy_handle)?;

        // init bars
        //

        // supporting window for NetWMCheck
        let wm_check_win = x::create_simple_window(&mut dpy_handle, drw.root, 0, 0, 1, 1, 0, 0, 0);
        let wm_name = cstr("delwm");

        x::change_property(
            &mut dpy_handle,
            wm_check_win,
            net_atoms.wm_check,
            x::XA_WINDOW,
            32,
            x::PROP_MODE_REPLACE,
            &(wm_check_win as u8),
            1,
        );
        x::change_property(
            &mut dpy_handle,
            wm_check_win,
            net_atoms.wm_name,
            utf8_string,
            8,
            x::PROP_MODE_REPLACE,
            wm_name.as_ptr() as _,
            wm_name.as_bytes().len() as i32,
        );
        x::change_property(
            &mut dpy_handle,
            drw.root,
            net_atoms.wm_check,
            x::XA_WINDOW,
            32,
            x::PROP_MODE_REPLACE,
            &(wm_check_win as u8),
            1,
        );

        // EWMH support per view
        x::change_property(
            &mut dpy_handle,
            drw.root,
            net_atoms.supported,
            x::XA_ATOM,
            32,
            x::PROP_MODE_REPLACE,
            &net_atoms as *const NetAtoms as _,
            struct_len::<NetAtoms, x::Atom>() as i32,
        );
        x::delete_property(&mut dpy_handle, drw.root, net_atoms.clients_list);

        // select events

        let mut window_attributes: x::SetWindowAttributes =
            unsafe { mem::MaybeUninit::zeroed().assume_init() };

        window_attributes.cursor = cursors.normal.xid;
        window_attributes.event_mask = x::SUBSTRUCTURE_REDIRECT_MASK
            | x::SUBSTRUCTURE_REDIRECT_MASK
            | x::BUTTON_PRESS_MASK
            | x::POINTER_MOTION_MASK
            | x::ENTER_WINDOW_MASK
            | x::LEAVE_WINDOW_MASK
            | x::STRUCTURE_NOTIFY_MASK
            | x::PROPERTY_CHANGE_MASK;

        x::change_window_attributes(
            &mut dpy_handle,
            drw.root,
            x::CW_EVENT_MASK | x::CW_CURSOR,
            &mut window_attributes,
        );

        dpy_handle.select_input(drw.root, window_attributes.event_mask);

        drop(dpy_handle);

        let mut wm = Self {
            bar_height,
            colorscheme,
            config,
            cursors,
            drw,
            lrpad,
            mons: Vec::new(),
            net_atoms,
            num_lock_mask: 0,
            root: 0,
            selmon: 0,
            wm_atoms,
            wm_check_win,
        };

        wm.update_geometry();
        wm.grab_keys();

        Ok(wm)
    }

    fn update_num_lock_mask(&mut self) {
        let mut dpy_ref = self.drw.dpy.borrow_mut();

        let mut num_lock_mask = 0;

        let modmap = x::ModifierKeymap::get(&mut dpy_ref);
        let keycodes = modmap.keycodes();
        let max_keypermod = modmap.max_keypermod();

        for i in 0..8 {
            for j in 0..max_keypermod {
                if keycodes[(i * max_keypermod + j) as usize]
                    == x::keysym_to_keycode(&mut dpy_ref, XK::NumLock)
                {
                    num_lock_mask = 1 << i;
                }
            }
        }

        println!("HERE: {:?}, {}, {}", keycodes, max_keypermod, num_lock_mask);

        self.num_lock_mask = num_lock_mask;
    }

    fn grab_keys(&mut self) {
        self.update_num_lock_mask();

        let mut dpy_ref = self.drw.dpy.borrow_mut();

        x::ungrab_key(&mut dpy_ref, x::AnyKey, x::AnyModifier, self.root);
        x::display_keycodes(&mut dpy_ref);
    }

    fn rect_to_monitor_idx(&self, x: i32, y: i32, w: i32, h: i32) -> usize {
        let mut ret = 0;
        let mut largest_area = 0;
        let mut area;

        for (i, m) in self.mons.iter().enumerate() {
            area = m.intersect(x, y, w, h);
            if largest_area < area {
                largest_area = area;
                ret = i;
            }
        }
        ret
    }

    fn window_to_client(&self, w: x::Window) -> Option<&Client> {
        for m in &self.mons {
            for c in m.clients.iter() {
                if c.window == w {
                    return Some(c);
                }
            }
        }
        None
    }

    fn window_to_monitor_idx(&mut self, w: x::Window) -> usize {
        if w == self.root {
            if let Some((x, y)) = self.get_pointer_root_xy() {
                return self.rect_to_monitor_idx(x, y, 1, 1);
            }
        }

        for (i, m) in self.mons.iter().enumerate() {
            if m.bar_window == w {
                return i;
            }
        }

        if let Some(c) = self.window_to_client(w) {
            return c.mon_idx;
        }

        self.selmon
    }

    fn update_geometry(&mut self) -> bool {
        let mut dpy_handle = self.drw.dpy.borrow_mut();

        if !xinerama::is_active(&mut dpy_handle) {
            unimplemented!("I need xinerama!!!");
        }

        // only consider unique geometries as separate screens
        let mut unique_screens = Vec::new();
        for info in xinerama::query_screens(&mut dpy_handle) {
            if is_unique_geometry(&info, &unique_screens) {
                unique_screens.push(info);
            }
        }

        let wm_mons_cnt = self.mons.len();
        let unique_cnt = unique_screens.len();

        drop(dpy_handle);

        // new monitors if wm_mons_cnt < unique_cnt
        for _ in 0..(unique_cnt - wm_mons_cnt) {
            self.mons
                .push(Monitor::new(self.drw.dpy.clone(), &self.config));
        }

        let mut dirty = false;

        // sync workspaces data with actual data
        for i in 0..unique_cnt {
            let m = &mut self.mons[i];
            let unique = unique_screens[i];
            if wm_mons_cnt <= i
                || unique.x_org != m.mon_x
                || unique.y_org != m.mon_y
                || unique.width != m.mon_width
                || unique.height != m.mon_height
            {
                dirty = true;
                m.num = i as u16;
                (m.mon_x, m.window_x) = (unique.x_org, unique.x_org);
                (m.mon_y, m.window_y) = (unique.y_org, unique.y_org);
                (m.mon_width, m.window_width) = (unique.width, unique.width);
                (m.mon_height, m.window_height) = (unique.height, unique.height);
                m.update_bar_pos(self.bar_height);
            }
        }

        // remove disconnected monitors
        for i in (unique_cnt - 1)..wm_mons_cnt {
            let mut m = self.mons.pop().unwrap();
            let clients = m.clients.take();
            self.mons[0].clients.extend(clients);

            if i == self.selmon {
                dirty = true;
            }
        }

        if dirty {
            self.selmon = 0;
            self.selmon = self.window_to_monitor_idx(self.root);
        }

        dirty

        // let mut unique_screens: Vec<xinerama::XineramaScreenInfo> = Vec::new();
        // let mut screens_cnt = 0;
        // let info = unsafe { xinerama::XineramaQueryScreens(dpy_ref, &mut screens_cnt) };

        // for i in 0..screens_cnt as isize {
        //     if is_unique_geom(unsafe { info.offset(i) }, &unique_screens) {
        //         unique_screens.push(unsafe { *info.offset(i) })
        //     } else {
        //     }
        // }
        // unsafe {
        //     xlib::XFree(info.cast());
        // }

        // let unique_screens_cnt = unique_screens.len();
        // let wm_screens_cnt = self.mons.len();

        // let mons = &mut self.mons;

        // for _ in 0..(unique_screens_cnt - wm_screens_cnt) {
        //     // add new screens if any
        //     mons.push(Workspace::new());
        // }

        // // sync workspaces data with actual data
        // for i in 0..unique_screens_cnt {
        //     let m = &mut mons[i];
        //     let unique = unique_screens[i];
        //     if wm_screens_cnt <= i
        //         || unique.x_org != m.mon_x
        //         || unique.y_org != m.mon_y
        //         || unique.width != m.mon_width
        //         || unique.height != m.mon_height
        //     {
        //         dirty = true;
        //         m.num = i as u16;
        //         (m.mon_x, m.window_x) = (unique.x_org, unique.x_org);
        //         (m.mon_y, m.window_y) = (unique.y_org, unique.y_org);
        //         (m.mon_width, m.window_width) = (unique.width, unique.width);
        //         (m.mon_height, m.window_height) = (unique.height, unique.height);
        //         m.update_bar_pos(self.bar_height);
        //     }
        // }

        // drop(dpy_borrow);

        // if dirty {
        //     self.selected_monitor = 0;
        //     self.selected_monitor = window_to_monitor_idx(self.root, self);
        // }

        // dirty
    }

    fn get_pointer_root_xy(&mut self) -> Option<(i32, i32)> {
        let mut dpy_handle = self.drw.dpy.borrow_mut();
        x::query_pointer(&mut dpy_handle, self.root).map(|p| (p.root_x, p.root_y))
    }
}

fn is_unique_geometry(info: &xinerama::ScreenInfo, unique_info: &[xinerama::ScreenInfo]) -> bool {
    unique_info.iter().all(|u_info| {
        !(u_info.x_org == info.x_org
            && u_info.y_org == info.y_org
            && u_info.width == info.width
            && u_info.height == info.height)
    })
}

// impl WM {
//     pub fn new(mut dpy: Display) -> Result<WM, &'static str> {
//         let utf8_string: x::Atom = x::intern_atom(&mut dpy, "UTF8_STRING", false);

//         let wm_atoms: [x::Atom; WM_ATOMS_LEN] = [
//             x::intern_atom(&mut dpy, "WM_PROTOCOLS", false),
//             x::intern_atom(&mut dpy, "WM_DELETE_WINDOW", false),
//             x::intern_atom(&mut dpy, "WM_STATE", false),
//             x::intern_atom(&mut dpy, "WM_TAKE_FOCUS", false),
//         ];

//         let net_atoms: [x::Atom; NET_ATOMS_LEN] = [
//             x::intern_atom(&mut dpy, "_NET_ACTIVE_WINDOW", false),
//             x::intern_atom(&mut dpy, "_NET_SUPPORTED", false),
//             x::intern_atom(&mut dpy, "_NET_NAME", false),
//             x::intern_atom(&mut dpy, "_NET_STATE", false),
//             x::intern_atom(&mut dpy, "_NET_SUPPORTING_WM_CHECK", false),
//             x::intern_atom(&mut dpy, "_NET_WM_STATE_FULLSCREEN", false),
//             x::intern_atom(&mut dpy, "_NET_WM_WINDOW_TYPE", false),
//             x::intern_atom(&mut dpy, "_NET_WM_WINDOW_TYPE_DIALOG", false),
//             x::intern_atom(&mut dpy, "_NET_CLIENT_LIST", false),
//         ];

//         let mut drw = drw::Drw::new(dpy, config::fonts)?;

//         let cursors = CursorModes {
//             normal: drw::Cursor::new(drw.dpy.clone(), 68),
//             resize: drw::Cursor::new(drw.dpy.clone(), 120),
//             drag: drw::Cursor::new(drw.dpy.clone(), 52),
//         };

//         let mut borrowed_dpy = drw.dpy.borrow_mut();

//         let scheme = Colorscheme {
//             normal: drw::create_colors(&config::colors.normal, drw.screen, &mut *borrowed_dpy)?,
//             selected: drw::create_colors(&config::colors.selected, drw.screen, &mut *borrowed_dpy)?,
//         };

//         let wm_check_window =
//             x::create_simple_window(&mut *borrowed_dpy, drw.root, 0, 0, 1, 1, 0, 0, 0);

//         let dpy: &mut Display = &mut *borrowed_dpy;

//         let lrpad = drw.fonts[0].h;
//         let bar_height = drw.fonts[0].h + 2;

//         // TODO(me): init bars

//         x::change_property(
//             dpy,
//             wm_check_window,
//             net_atoms[NetAtom::WMCheck as usize],
//             xlib::XA_WINDOW,
//             32,
//             xlib::PropModeReplace,
//             &(wm_check_window as u8),
//             1,
//         );
//         x::change_property(
//             dpy,
//             wm_check_window,
//             net_atoms[NetAtom::WMName as usize],
//             utf8_string,
//             8,
//             xlib::PropModeReplace,
//             cstr("dwm").as_ptr() as *const u8,
//             3,
//         );
//         x::change_property(
//             dpy,
//             drw.root,
//             net_atoms[NetAtom::WMCheck as usize],
//             xlib::XA_WINDOW,
//             32,
//             xlib::PropModeReplace,
//             &(wm_check_window as u8),
//             1,
//         );
//         x::change_property(
//             dpy,
//             drw.root,
//             net_atoms[NetAtom::Supported as usize],
//             xlib::XA_ATOM,
//             32,
//             xlib::PropModeReplace,
//             net_atoms.as_ptr() as *const u8,
//             NET_ATOMS_LEN as i32,
//         );

//         x::delete_property(dpy, drw.root, net_atoms[NetAtom::ClientList as usize]);

//         let mut window_attributes: xlib::XSetWindowAttributes =
//             unsafe { mem::MaybeUninit::zeroed().assume_init() };

//         window_attributes.cursor = cursors.normal.xid;
//         window_attributes.event_mask = xlib::SubstructureRedirectMask
//             | xlib::SubstructureNotifyMask
//             | xlib::ButtonPressMask
//             | xlib::PointerMotionMask
//             | xlib::EnterWindowMask
//             | xlib::LeaveWindowMask
//             | xlib::StructureNotifyMask
//             | xlib::PropertyChangeMask;
//         unsafe {
//             xlib::XChangeWindowAttributes(
//                 dpy.deref_mut(),
//                 drw.root,
//                 xlib::CWEventMask | xlib::CWCursor,
//                 &mut window_attributes,
//             );
//             xlib::XSelectInput(dpy.deref_mut(), drw.root, window_attributes.event_mask);
//         };

//         drop(borrowed_dpy);

//         let mut wm = WM {
//             drw,
//             mons: Vec::new(),
//             bar_height,
//             lrpad,
//             selected_monitor: 0,
//             root: 0,
//             wm_atoms,
//             net_atoms,
//             cursors,
//             scheme,
//             wm_check_window,
//             num_lock_mask: 0,
//         };

//         wm.update_geometry();
//         // wm.grab_keys();
//         // wm.focus();

//         Ok(wm)
//     }

//     // fn focus(&mut self, client_id: usize) {}

//     // fn focus_any(&mut self) {
//     //     let mon = &self.mons[self.selected_monitor];

//     //     match mon.clients_stack.last() {
//     //         Some(&client_id) => self.focus(client_id as usize),
//     //         None => unsafe {
//     //             let mut dpy_borrow = self.drw.dpy.borrow_mut();
//     //             let dpy_ref = dpy_borrow.as_mut();
//     //             xlib::XSetInputFocus(
//     //                 dpy_ref,
//     //                 self.root,
//     //                 xlib::RevertToPointerRoot,
//     //                 xlib::CurrentTime,
//     //             );
//     //             xlib::XDeleteProperty(
//     //                 dpy_ref,
//     //                 self.root,
//     //                 self.net_atoms[NetAtom::ActiveWindow as usize],
//     //             );
//     //         },
//     //     }

//     //     // let selmon = &self.mons[self.selected_monitor];
//     //     // if selmon.selected_client != client_id {
//     //     //     self.unfocus(selmon.selected_client, false);
//     //     // }
//     // }

//     // fn unfocus(&mut self, client_id: usize, set_focus: bool) {
//     //     self.grab_buttons(client_id, false);
//     // }

//     fn grab_buttons(&mut self, client: &mut Client, focused: bool) {
//         self.num_lock_mask = update_num_lock_mask(&mut self.drw.dpy);

//         let mut dpy_borrow = self.drw.dpy.borrow_mut();
//         let dpy_ref = dpy_borrow.as_mut();

//         let modifiers = [
//             0,
//             xlib::LockMask,
//             self.num_lock_mask,
//             self.num_lock_mask | xlib::LockMask,
//         ];

//         unsafe {
//             xlib::XUngrabButton(
//                 dpy_ref,
//                 xlib::AnyButton as u32,
//                 xlib::AnyModifier,
//                 client.window,
//             );
//         }

//         if !focused {
//             unsafe {
//                 xlib::XGrabButton(
//                     dpy_ref,
//                     xlib::AnyButton as u32,
//                     xlib::AnyModifier,
//                     client.window,
//                     False,
//                     BUTTOM_MASK,
//                     xlib::GrabModeSync,
//                     xlib::GrabModeSync,
//                     0,
//                     0,
//                 );
//             }
//         }

//         for button in config::buttons {
//             if button.click == Click::ClientWin {
//                 for modifier in modifiers {
//                     unsafe {
//                         xlib::XGrabButton(
//                             dpy_ref,
//                             button.button,
//                             button.mask | modifier,
//                             client.window,
//                             False,
//                             BUTTOM_MASK,
//                             xlib::GrabModeAsync,
//                             xlib::GrabModeSync,
//                             0,
//                             0,
//                         );
//                     }
//                 }
//             }
//         }
//     }

//     fn grab_keys(&mut self) {
//         self.num_lock_mask = update_num_lock_mask(&mut self.drw.dpy);
//         let mut dpy_borrow = self.drw.dpy.borrow_mut();
//         let dpy_ref = dpy_borrow.as_mut();

//         let modifiers = [
//             0,
//             xlib::LockMask,
//             self.num_lock_mask,
//             self.num_lock_mask | xlib::LockMask,
//         ];

//         let mut start = 0;
//         let mut end = 0;
//         let mut skip = 0;
//         let syms;

//         unsafe {
//             xlib::XUngrabKey(dpy_ref, xlib::AnyKey, xlib::AnyModifier, self.root);
//             xlib::XDisplayKeycodes(dpy_ref, &mut start, &mut end);
//             syms = xlib::XGetKeyboardMapping(dpy_ref, start as u8, end - start + 1, &mut skip);
//             if syms.is_null() {
//                 return;
//             }
//             for k in start..=end {
//                 for key in config::keys {
//                     if key.keysym == *syms.offset(((k - start) * skip) as isize) as u32 {
//                         for modifier in modifiers {
//                             xlib::XGrabKey(
//                                 dpy_ref,
//                                 k,
//                                 key.modkey | modifier,
//                                 self.root,
//                                 True,
//                                 xlib::GrabModeAsync,
//                                 xlib::GrabModeAsync,
//                             );
//                         }
//                     }
//                 }
//             }

//             xlib::XFree(syms as *mut libc::c_void);
//         }
//     }
// }

fn sigchld() {
    unsafe {
        if libc::signal(libc::SIGCHLD, sigchld as usize) == libc::SIG_ERR {
            err!("can't install SIGCHLD handler:");
            libc::exit(1)
        }
        // 1 = WHOHANG
        while 0 < libc::waitpid(-1, ptr::null_mut(), 1) {}
    }
}

// fn update_num_lock_mask(dpy: &mut DisplayRc) -> u32 {
//     let mut dpy_borrow = dpy.borrow_mut();
//     let dpy_ref = dpy_borrow.as_mut();

//     let mut num_lock_mask = 0;
//     let modmap = unsafe { xlib::XGetModifierMapping(dpy_ref) };
//     unsafe {
//         for i in 0..8 {
//             for j in 0..((*modmap).max_keypermod) {
//                 if *(*modmap)
//                     .modifiermap
//                     .offset((i * (*modmap).max_keypermod + j) as isize)
//                     == xlib::XKeysymToKeycode(dpy_ref, keysym::XK_Num_Lock as u64)
//                 {
//                     num_lock_mask = 1 << i;
//                 }
//             }
//         }
//         xlib::XFreeModifiermap(modmap);
//     }
//     num_lock_mask
// }

// fn window_to_client(window: xlib::Window, monitors: &[Workspace]) -> Option<&Client> {
//     for mon in monitors {
//         for client in &mon.clients {
//             if client.window == window {
//                 return Some(client);
//             }
//         }
//     }
//     None
// }

// fn window_to_monitor_idx(window: xlib::Window, wm: &mut WM) -> usize {
//     let relative_pos = get_root_pointer(&mut wm.drw.dpy, wm.root);

//     if let Some((x, y)) = relative_pos {
//         if window == wm.root {
//             return rect_to_monitor(wm, x, y, 1, 1);
//         }
//     }

//     for (i, mon) in wm.mons.iter().enumerate() {
//         if window == mon.bar_window {
//             return i;
//         }
//     }
//     if let Some(client) = window_to_client(window, &wm.mons) {
//         return client.mon_idx;
//     }

//     wm.selected_monitor
// }
