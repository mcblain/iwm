// use x11::xlib;
use crate::x;

// use crate::workspace::Workspace;

#[derive(Debug)]
pub struct Client {
    name: String,
    mina: f32,
    maxa: f32,
    x: i32,
    y: i32,
    w: i32,
    h: i32,
    oldx: i32,
    oldy: i32,
    oldw: i32,
    oldh: i32,
    basew: i32,
    baseh: i32,
    incw: i32,
    inch: i32,
    maxw: i32,
    maxh: i32,
    minw: i32,
    minh: i32,
    hintsvalid: i32,
    bw: i32,
    oldbw: i32,
    tags: u32,
    isfixed: bool,
    isfloating: bool,
    isurgent: bool,
    neverfocus: bool,
    oldstate: bool,
    isfullscreen: bool,
    pub window: x::Window,
    pub mon_idx: usize,
}

// impl Client {
//     fn is_visible(&self, mons: &[Workspace]) -> bool {
//         let mon = &mons[self.mon_idx];
//         self.tags & mon.tagset[mon.seltags] != 0
//     }
// }
