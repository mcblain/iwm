use dwm_rs::{
    dummy_arrange, dummy_btn_func, spawn, Arg, Button, Click, Colorscheme, ColorschemeModes,
    Config, Key, Layout,
};
use x11::{
    keysym::XK_Return,
    xlib::{Button1, Mod1Mask, Mod4Mask, ShiftMask},
};

pub fn make_config() -> Config {
    let col_gray1 = "#16161D";
    let col_gray2 = "#363646";
    let col_gray3 = "#7AA89F";
    let col_gray4 = "#E6C384";
    let col_sel_bg = "#1F1F28";

    // appearance
    let border_width = 1;
    let snap = 32;
    let show_bar = true;
    let top_bar = true;
    let fonts = vec!["monospace:size=10"];
    let dmenu_font = "monospace:size=10";

    let colorscheme = ColorschemeModes {
        normal: Colorscheme {
            fg: col_gray3,
            bg: col_gray1,
            border: col_gray2,
        },
        selected: Colorscheme {
            fg: col_gray4,
            bg: col_sel_bg,
            border: col_gray4,
        },
    };

    // tagging
    let tags = vec!["1", "2", "3", "4", "5", "6", "7", "8", "9"];

    // layouts
    let master_area_factor = 0.55;
    let master_cnt = 1;
    let resize_hints = true;
    let lock_fullscreen = true;

    let layouts = vec![
        Layout {
            symbol: "[]=",
            func: dummy_arrange,
        },
        Layout {
            symbol: "><>",
            func: dummy_arrange,
        },
        Layout {
            symbol: "[M]",
            func: dummy_arrange,
        },
    ];

    // key difenitions
    let mod_key = Mod4Mask;
    let alt_key = Mod1Mask;

    // commands
    let dmenu_cmd = vec![
        "dmenu_run",
        "-nb",
        col_gray1,
        "-nf",
        col_gray3,
        "-sb",
        col_sel_bg,
        "-sf",
        col_gray4,
    ];
    let keys = vec![Key {
        mod_key: mod_key | ShiftMask,
        key_sym: XK_Return,
        func: spawn,
        arg: Arg::Cmd(dmenu_cmd),
    }];

    // button definitions
    let buttons = vec![Button {
        click: Click::LtSymbol,
        mask: 0,
        func: dummy_btn_func,
        button: Button1,
        arg: Arg::Int(1),
    }];

    Config {
        border_width,
        snap,
        show_bar,
        top_bar,
        fonts,
        tags,
        master_area_factor,
        master_cnt,
        resize_hints,
        lock_fullscreen,
        layouts,
        keys,
        buttons,
        colorscheme,
    }
}
