mod keysym;

pub mod xft;
pub mod xinerama;

use crate::util::cstr;
use std::{
    cell::{RefCell, RefMut},
    ffi::{c_int, c_long, c_uint, c_ulong, c_void},
    fmt::Debug,
    marker,
    ops::{Deref, DerefMut},
    ptr,
    rc::Rc,
    slice,
};
use x11::xlib;

pub use keysym::XK;
pub use xlib::{
    AnyKey, AnyModifier, Atom, BadAccess as BAD_ACCESS, BadDrawable as BAD_DRAWABLE,
    BadMatch as BAD_MATCH, BadWindow as BAD_WINDOW, ButtonPressMask as BUTTON_PRESS_MASK,
    CWCursor as CW_CURSOR, CWEventMask as CW_EVENT_MASK, Cursor, Display as DisplayRaw, Drawable,
    EnterWindowMask as ENTER_WINDOW_MASK, KeyCode, LeaveWindowMask as LEAVE_WINDOW_MASK,
    PointerMotionMask as POINTER_MOTION_MASK, PropModeReplace as PROP_MODE_REPLACE,
    PropertyChangeMask as PROPERTY_CHANGE_MASK, StructureNotifyMask as STRUCTURE_NOTIFY_MASK,
    SubstructureNotifyMask as SUBSTRUCTURE_NOTIFY_MASK,
    SubstructureRedirectMask as SUBSTRUCTURE_REDIRECT_MASK, Window, XErrorEvent as ErrorEvent,
    XGCValues as GCValues, XSetWindowAttributes as SetWindowAttributes, GC, XA_ATOM, XA_WINDOW,
};

pub type ErrorHandler = unsafe extern "C" fn(*mut DisplayRaw, *mut ErrorEvent) -> i32;
pub struct Display(&'static mut DisplayRaw);

impl Display {
    pub fn new() -> Option<Self> {
        open_display("").map(|dpy_ref| Display(dpy_ref))
    }

    pub fn as_mut(&mut self) -> &'_ mut DisplayRaw {
        self.0
    }

    pub fn default_root_window(&mut self) -> u64 {
        unsafe { xlib::XDefaultRootWindow(self.0) }
    }

    pub fn default_screen(&mut self) -> i32 {
        unsafe { xlib::XDefaultScreen(self.0) }
    }

    pub fn select_input(&mut self, w: u64, event_mask: i64) -> i32 {
        unsafe { xlib::XSelectInput(self.0, w, event_mask) }
    }

    pub fn sync(&mut self, discard: bool) -> i32 {
        unsafe { xlib::XSync(self.0, discard as c_int) }
    }

    pub fn width(&mut self, screen: i32) -> u32 {
        unsafe { xlib::XDisplayWidth(self.0, screen) as c_uint }
    }

    pub fn height(&mut self, screen: i32) -> u32 {
        unsafe { xlib::XDisplayHeight(self.0, screen) as c_uint }
    }

    pub fn root_window(&mut self, screen: i32) -> u64 {
        unsafe { xlib::XRootWindow(self.0, screen) }
    }

    pub fn default_depth(&mut self, screen: i32) -> u32 {
        unsafe { xlib::XDefaultDepth(self.0, screen) as c_uint }
    }

    pub fn create_pixmap(
        &mut self,
        drawable: xlib::Drawable,
        w: c_uint,
        h: c_uint,
        depth: c_uint,
    ) -> c_ulong {
        unsafe { xlib::XCreatePixmap(self.0, drawable, w, h, depth) }
    }

    pub fn create_gc(
        &mut self,
        drawable: Drawable,
        value_mask: c_ulong,
        values: Option<&mut GCValues>,
    ) -> GC {
        let values_ptr = match values {
            Some(v) => v,
            None => ptr::null_mut(),
        };
        unsafe { xlib::XCreateGC(self.deref_mut(), drawable, value_mask, values_ptr) }
    }
}

impl Deref for Display {
    type Target = xlib::_XDisplay;
    fn deref(&self) -> &Self::Target {
        self.0
    }
}

impl DerefMut for Display {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.0
    }
}

impl Debug for Display {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "_XDisplay")
    }
}

impl Drop for Display {
    fn drop(&mut self) {
        unsafe { xlib::XCloseDisplay(self.0) };
    }
}

#[derive(Debug, Clone)]
pub struct DisplayRc(Rc<RefCell<Display>>);

impl DisplayRc {
    pub fn new(dpy: Display) -> Self {
        Self(Rc::new(RefCell::new(dpy)))
    }

    pub fn borrow_mut(&mut self) -> RefMut<Display> {
        self.0.borrow_mut()
    }
}

pub struct ModifierKeymapDeref<'a> {
    modifiermap: &'a [KeyCode],
    max_keypermod: i32,
}

#[derive(Debug)]
pub struct ModifierKeymap {
    modmap: *mut xlib::XModifierKeymap,
}

impl<'a> ModifierKeymap {
    pub fn get(dpy: &mut Display) -> Self {
        ModifierKeymap {
            modmap: get_modifier_mapping(dpy),
        }
    }

    pub fn keycodes(&self) -> &[KeyCode] {
        unsafe {
            slice::from_raw_parts(
                (*self.modmap).modifiermap as *const _,
                (self.max_keypermod() * 8) as usize,
            )
        }
    }

    pub fn max_keypermod(&self) -> i32 {
        unsafe { (*self.modmap).max_keypermod }
    }
}

impl<'a> Drop for ModifierKeymap {
    fn drop(&mut self) {
        unsafe { free_modifier_map(self.modmap) };
    }
}

pub fn set_error_handler(handler: ErrorHandler) -> Option<ErrorHandler> {
    unsafe { xlib::XSetErrorHandler(Some(handler)) }
}

pub fn open_display(display_name: &str) -> Option<&'static mut DisplayRaw> {
    let c_name = cstr(display_name);
    unsafe { xlib::XOpenDisplay(c_name.as_ptr()).as_mut() }
}

pub fn supports_locale() -> bool {
    unsafe { xlib::XSupportsLocale() != 0 }
}

pub fn intern_atom(dpy: &mut Display, atom_name: &str, only_if_exists: bool) -> c_ulong {
    let c_name = cstr(atom_name);
    unsafe { xlib::XInternAtom(dpy.deref_mut(), c_name.as_ptr(), only_if_exists as c_int) }
}

pub fn create_simple_window(
    dpy: &mut Display,
    parent: xlib::Window,
    x: c_int,
    y: c_int,
    width: c_uint,
    height: c_uint,
    border_width: c_uint,
    border: c_ulong,
    background: c_ulong,
) -> c_ulong {
    unsafe {
        xlib::XCreateSimpleWindow(
            dpy.deref_mut(),
            parent,
            x,
            y,
            width,
            height,
            border_width,
            border,
            background,
        )
    }
}

pub fn default_visual<'a>(dpy: &'a mut Display, screen: c_int) -> &'a mut xlib::Visual {
    unsafe { xlib::XDefaultVisual(dpy.as_mut(), screen).as_mut().unwrap() }
}

pub fn default_colormap(dpy: &mut Display, screen: c_int) -> xlib::Colormap {
    unsafe { xlib::XDefaultColormap(dpy.as_mut(), screen) }
}

pub fn set_line_attributes(
    dpy: &mut Display,
    gc: GC,
    line_width: c_uint,
    line_style: c_int,
    cap_style: c_int,
    join_style: c_int,
) -> c_int {
    unsafe {
        xlib::XSetLineAttributes(
            dpy.deref_mut(),
            gc,
            line_width,
            line_style,
            cap_style,
            join_style,
        )
    }
}

pub fn change_property(
    dpy: &mut Display,
    w: Window,
    property: Atom,
    type_: Atom,
    format: c_int,
    mode: c_int,
    data: *const u8,
    nelements: c_int,
) -> i32 {
    unsafe {
        xlib::XChangeProperty(
            dpy.deref_mut(),
            w,
            property,
            type_,
            format,
            mode,
            data,
            nelements,
        )
    }
}

pub fn delete_property(dpy: &mut Display, w: Window, property: Atom) -> i32 {
    unsafe { xlib::XDeleteProperty(dpy.deref_mut(), w, property) }
}

pub fn create_font_cursor(dpy: &mut Display, shape: u32) -> u64 {
    unsafe { xlib::XCreateFontCursor(dpy.deref_mut(), shape) }
}

pub fn free_cursor(dpy: &mut Display, cursor: Cursor) -> i32 {
    unsafe { xlib::XFreeCursor(dpy.deref_mut(), cursor) }
}

pub fn change_window_attributes(
    dpy: &mut Display,
    w: Window,
    valuemask: u64,
    attributes: &mut SetWindowAttributes,
) -> i32 {
    unsafe { xlib::XChangeWindowAttributes(dpy.deref_mut(), w, valuemask, attributes) }
}

pub unsafe fn free(v: *mut c_void) -> i32 {
    unsafe { xlib::XFree(v) }
}

pub fn unmap_window(dpy: &mut Display, w: Window) -> i32 {
    unsafe { xlib::XUnmapWindow(dpy.deref_mut(), w as c_ulong) }
}

pub fn destroy_window(dpy: &mut Display, w: Window) -> i32 {
    unsafe { xlib::XDestroyWindow(dpy.deref_mut(), w as c_ulong) }
}

#[derive(Default)]
pub struct PointerInfo {
    pub root: Window,
    pub child: Window,
    pub root_x: i32,
    pub root_y: i32,
    pub win_x: i32,
    pub win_y: i32,
    pub mask: u32,
}

pub fn query_pointer(dpy: &mut Display, w: Window) -> Option<PointerInfo> {
    let mut info = PointerInfo::default();
    let success = unsafe {
        xlib::XQueryPointer(
            dpy.deref_mut(),
            w,
            &mut info.root,
            &mut info.child,
            &mut info.root_x,
            &mut info.root_y,
            &mut info.win_x,
            &mut info.win_y,
            &mut info.mask,
        ) != 0
    };

    if success {
        Some(info)
    } else {
        None
    }
}

pub fn get_modifier_mapping(dpy: &mut Display) -> *mut xlib::XModifierKeymap {
    unsafe { xlib::XGetModifierMapping(dpy.deref_mut()) }
}

pub unsafe fn free_modifier_map(modmap: *mut xlib::XModifierKeymap) -> i32 {
    xlib::XFreeModifiermap(modmap)
}

pub fn keysym_to_keycode(dpy: &mut Display, keysym: XK) -> KeyCode {
    unsafe { xlib::XKeysymToKeycode(dpy.deref_mut(), keysym as xlib::KeySym) }
}

pub fn ungrab_key(dpy: &mut Display, keycode: i32, modifiers: u32, grab_window: Window) -> i32 {
    unsafe { xlib::XUngrabKey(dpy.deref_mut(), keycode, modifiers, grab_window) }
}

pub fn display_keycodes(dpy: &mut Display) -> (i32, i32) {
    let mut min_keycodes = 0;
    let mut max_keycodes = 0;
    unsafe { xlib::XDisplayKeycodes(dpy.deref_mut(), &mut min_keycodes, &mut max_keycodes) };
    (min_keycodes, max_keycodes)
}

pub fn get_keyboard_mapping<'a>(
    dpy: &mut Display,
    first_keycode: i32,
    keycode_count: i32,
    keysym_per_keycode_return: &mut xlib::KeySym,
) -> &mut xlib::KeySym {
    unsafe {
        xlib::XGetKeyboardMapping(
            dpy.deref_mut(),
            start,
            end - start,
            keysym_per_keycode_return,
        )
    }
}
