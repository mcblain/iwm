use std::{ffi::c_int, ops::DerefMut};

use crate::util::cstr;

use super::Display;

use x11::xlib;

pub use x11::xft::{self, FcPattern, XftColor as Color, XftFont as Font};

pub fn color_alloc_name(
    dpy: &mut Display,
    visual: &mut xlib::Visual,
    cmap: xlib::Colormap,
    name: &str,
    result: &mut Color,
) -> bool {
    let c_name = cstr(name);
    unsafe { xft::XftColorAllocName(dpy.deref_mut(), visual, cmap, c_name.as_ptr(), result) != 0 }
}

pub fn color_alloc_default(
    dpy: &mut Display,
    screen: c_int,
    name: &str,
    result: &mut Color,
) -> bool {
    let c_name = cstr(name);
    let colormap = super::default_colormap(dpy, screen);
    let visual = unsafe { xlib::XDefaultVisual(dpy.as_mut(), screen).as_mut().unwrap() };
    unsafe { xft::XftColorAllocName(dpy.as_mut(), visual, colormap, c_name.as_ptr(), result) != 0 }
}

pub fn font_open_name(dpy: &mut Display, screen: i32, name: &str) -> Option<&'static mut Font> {
    let c_name = cstr(name);
    unsafe { xft::XftFontOpenName(dpy.deref_mut(), screen, c_name.as_ptr()).as_mut() }
}

pub fn font_open_pattern(dpy: &mut Display, pattern: &mut FcPattern) -> Option<&'static mut Font> {
    unsafe { xft::XftFontOpenPattern(dpy.deref_mut(), pattern).as_mut() }
}

pub fn font_close(dpy: &mut Display, xfont: &mut Font) {
    unsafe { xft::XftFontClose(dpy.deref_mut(), xfont) }
}

pub fn name_parse(name: &str) -> Option<&'static mut FcPattern> {
    let c_name = cstr(name);
    unsafe { xft::XftNameParse(c_name.as_ptr()).as_mut() }
}
