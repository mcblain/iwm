use std::ops::DerefMut;

use super::Display;
use x11::xinerama;

pub use xinerama::XineramaScreenInfo as ScreenInfo;

pub fn is_active(dpy: &mut Display) -> bool {
    unsafe { xinerama::XineramaIsActive(dpy.deref_mut()) != 0 }
}

pub fn query_screens(dpy: &mut Display) -> Vec<ScreenInfo> {
    unsafe {
        let mut scr_num = 0;
        let info_ptr = xinerama::XineramaQueryScreens(dpy.deref_mut(), &mut scr_num);
        let mut screens_info = Vec::with_capacity(scr_num as usize);
        for i in 0..(scr_num as isize) {
            screens_info.push(*info_ptr.offset(i));
        }
        super::free(info_ptr as *mut libc::c_void);
        screens_info
    }
}
