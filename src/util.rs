pub fn cstr(str: &str) -> std::ffi::CString {
    unsafe { std::ffi::CString::from_vec_unchecked(format!("{}\0", str).into()) }
}

pub fn struct_len<Struct, Elem>() -> usize {
    std::mem::size_of::<Struct>() / std::mem::size_of::<Elem>()
}

#[macro_export]
macro_rules! warn {
    ($($arg:tt)*) => {{
        eprintln!("delwm[WARN]: {}", ($($arg)*));
    }};
}

#[macro_export]
macro_rules! err {
    ($($arg:tt)*) => {{
        eprintln!("delwm[ERR]: {}", ($($arg)*));
    }};
}

#[macro_export]
macro_rules! info {
    ($($arg:tt)*) => {{
        eprintln!("delwm[INFO]: {}", ($($arg)*));
    }};
}

#[macro_export]
macro_rules! debug {
    ($($arg:tt)*) => {{
        use std::io::Write;
        std::io::stdout().lock().flush().unwrap();
        eprintln!("delwm[DEGUB]: {}", ($($arg)*));
    }};
}
