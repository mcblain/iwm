use crate::x::{self, Display};
use std::{
    cell::{RefCell, RefMut},
    fmt::Debug,
    mem,
    ops::DerefMut,
    rc::Rc,
};
use x::{xft, DisplayRc};
use x11::{self, xlib, xrender};

macro_rules! mut_dpy {
    ($dpy:expr) => {
        $dpy.borrow_mut().as_mut()
    };
}

#[derive(Debug)]
pub struct Colorscheme<T> {
    pub fg: T,
    pub bg: T,
    pub border: T,
}

impl Colorscheme<Color> {
    pub fn new(
        colors: &Colorscheme<&str>,
        screen: i32,
        dpy: &mut Display,
    ) -> Result<Self, &'static str> {
        Ok(Colorscheme {
            fg: Color::new(colors.fg, screen, dpy)?,
            bg: Color::new(colors.bg, screen, dpy)?,
            border: Color::new(colors.border, screen, dpy)?,
        })
    }
}

#[derive(Debug)]
pub struct ColorschemeModes<T> {
    pub normal: Colorscheme<T>,
    pub selected: Colorscheme<T>,
}

impl ColorschemeModes<Color> {
    pub fn new(
        color_names: &ColorschemeModes<&str>,
        screen: i32,
        dpy: &mut x::Display,
    ) -> Result<Self, &'static str> {
        Ok(Self {
            normal: Colorscheme::new(&color_names.normal, screen, dpy)?,
            selected: Colorscheme::new(&color_names.selected, screen, dpy)?,
        })
    }
}

#[derive(Debug)]
pub struct Color {
    pub pixel: u64,
    pub color: xrender::XRenderColor,
}

impl Color {
    pub fn new(colorname: &str, screen: i32, dpy: &mut Display) -> Result<Self, &'static str> {
        let mut clr: xft::Color = unsafe { mem::MaybeUninit::zeroed().assume_init() };
        let success = x::xft::color_alloc_default(dpy, screen, colorname, &mut clr);

        if success {
            Ok(Self {
                pixel: clr.pixel,
                color: clr.color,
            })
        } else {
            Err("cannot allocate color")
        }
    }
}

pub struct Font {
    pub h: i16,
    pub xfont: &'static mut xft::Font,
    pub pattern: &'static mut xft::FcPattern,
    dpy: DisplayRc,
}

impl Font {
    pub fn from_font_name<'b>(
        font_name: &'b str,
        screen: i32,
        mut dpy: DisplayRc,
    ) -> Result<Self, String> {
        let mut dpy_handle = dpy.borrow_mut();

        let xfont = match xft::font_open_name(&mut dpy_handle, screen, font_name) {
            Some(xfont) => xfont,
            _ => return Err(format!("cannot load font from name: '{font_name}'")),
        };

        let pattern = match xft::name_parse(font_name) {
            Some(pattern) => pattern,
            _ => {
                xft::font_close(&mut dpy_handle, xfont);
                return Err(format!("cannot parse font name to pattern: '{font_name}'"));
            }
        };

        let h = (xfont.ascent + xfont.descent) as i16;

        drop(dpy_handle);

        Ok(Self {
            xfont,
            pattern,
            h,
            dpy,
        })
    }

    pub fn from_pattern(
        pattern: &'static mut xft::FcPattern,
        dpy: &mut DisplayRc,
    ) -> Result<Self, &'static str> {
        let xfont = match xft::font_open_pattern(&mut dpy.borrow_mut(), pattern) {
            Some(xfont) => xfont,
            _ => return Err("cannot load font from pattern"),
        };

        let h = ((*xfont).ascent + (*xfont).descent) as i16;

        Ok(Self {
            xfont,
            pattern,
            h,
            dpy: dpy.clone(),
        })
    }
}

impl Drop for Font {
    fn drop(&mut self) {
        let mut dpy = self.dpy.borrow_mut();
        xft::font_close(&mut dpy, self.xfont)
    }
}

impl Debug for Font {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Font")
    }
}

#[derive(Debug)]
pub struct Drw {
    pub w: u32,
    pub h: u32,
    pub dpy: DisplayRc,
    pub screen: i32,
    pub root: xlib::Window,
    pub drawable: xlib::Drawable,
    pub gc: xlib::GC,
    pub scheme: Option<Colorscheme<Color>>,
    pub fonts: Vec<Font>,
}

impl Drw {
    pub fn new<'b>(mut dpy: Display, screen: i32) -> Self {
        let w = dpy.width(screen);
        let h = dpy.height(screen);
        let root = dpy.root_window(screen);
        let depth = dpy.default_depth(screen);
        let drawable = dpy.create_pixmap(root, w, h, depth);
        let gc = dpy.create_gc(root, 0, None);

        Self {
            dpy: DisplayRc::new(dpy),
            root,
            screen,
            w,
            h,
            drawable,
            gc,
            fonts: Vec::new(),
            scheme: None,
        }

        // let depth = x::default_depth(&mut dpy, screen);
        // let drawable = x::create_pixmap(&mut dpy, root, w, h, depth);
        // let gc = x::create_gc(&mut dpy, root, 0, None);
        // let scheme = create_colors(
        //     &Colors {
        //         fg: "#000000",
        //         bg: "#000000",
        //         border: "#000000",
        //     },
        //     screen,
        //     &mut dpy,
        // )?;

        // x::set_line_attributes(
        //     &mut dpy,
        //     gc,
        //     1,
        //     xlib::LineSolid,
        //     xlib::CapButt,
        //     xlib::JoinMiter,
        // );

        // let dpy_rc = DisplayRc::new(dpy);
        // let fonts = create_fontset(fontnames, screen, dpy_rc.clone())?;

        // Ok(Self {
        //     dpy: dpy_rc,
        //     screen,
        //     root,
        //     w,
        //     h,
        //     drawable,
        //     gc,
        //     scheme,
        //     fonts,
        // })
    }

    pub fn create_fontset(&mut self, fontnames: &[&str]) -> Result<Vec<Font>, String> {
        fontnames
            .iter()
            .map(|&name| Font::from_font_name(name, self.screen, self.dpy.clone()))
            .collect()
    }

    pub fn drw_map(&mut self, win: xlib::Window, x: i32, y: i32, w: u32, h: u32) {
        let mut borrow = self.dpy.borrow_mut();
        let dpy_ref = borrow.as_mut();

        unsafe {
            xlib::XCopyArea(dpy_ref, self.drawable, win, self.gc, x, y, w, h, x, y);
            xlib::XSync(dpy_ref, xlib::False);
        };
    }

    pub fn resize(&mut self, w: u32, h: u32) {
        let mut borrow = self.dpy.borrow_mut();
        let dpy_ref = borrow.as_mut();

        self.w = w;
        self.h = h;
        unsafe {
            if self.drawable != 0 {
                xlib::XFreePixmap(dpy_ref, self.drawable);
            }
            self.drawable = xlib::XCreatePixmap(
                dpy_ref,
                self.root,
                w,
                h,
                xlib::XDefaultDepth(dpy_ref, self.screen) as u32,
            );
        }
    }

    // pub fn draw_rect(&mut self, x: i32, y: i32, w: u32, h: u32, fill: bool, invert: bool) {
    //     let mut borrow = self.dpy.borrow_mut();
    //     let dpy_ref = borrow.as_mut();

    //     unsafe {
    //         xlib::XSetForeground(
    //             dpy_ref,
    //             self.gc,
    //             if invert {
    //                 self.scheme.bg.pixel
    //             } else {
    //                 self.scheme.fg.pixel
    //             },
    //         );
    //         if fill {
    //             xlib::XFillRectangle(dpy_ref, self.drawable, self.gc, x, y, w, h);
    //         } else {
    //             xlib::XDrawRectangle(dpy_ref, self.drawable, self.gc, x, y, w, h);
    //         }
    //     }
    // }
}

impl Drop for Drw {
    fn drop(&mut self) {
        let mut borrow = self.dpy.borrow_mut();
        let dpy_ref = borrow.as_mut();

        unsafe {
            xlib::XFreePixmap(dpy_ref, self.drawable);
            xlib::XFreeGC(dpy_ref, self.gc);
        }
    }
}

#[derive(Debug)]
pub struct Cursor {
    pub xid: xlib::Cursor,
    dpy: DisplayRc,
}

impl Cursor {
    pub fn new(mut dpy: DisplayRc, shape: u32) -> Self {
        let xid = x::create_font_cursor(&mut dpy.borrow_mut(), shape);
        Self { xid, dpy }
    }
}

impl Drop for Cursor {
    fn drop(&mut self) {
        unsafe {
            xlib::XFreeCursor(mut_dpy!(self.dpy), self.xid);
        }
    }
}
