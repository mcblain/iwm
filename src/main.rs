#![allow(dead_code)]
#![allow(non_upper_case_globals)]

use dwm_rs::{supports_locale, WM};

#[macro_use]
mod util;
mod config;
mod x;

fn main() {
    println!("HERE");

    if let Some(key) = &std::env::args().nth(1) {
        if key == "-v" {
            println!("delwm-0.1");
        } else {
            eprintln!("usage: delwm [-v]");
        }
        return;
    };

    if !supports_locale() {
        warn!("no locale support");
    }

    let config = config::make_config();

    let wm = WM::new(config);

    println!("WM STATE {:?}", wm);

    println!("AND HERE");

    // let _wm = match WM::new(dpy) {
    //     Ok(wm) => wm,
    //     Err(err) => {
    //         eprintln!("setup error: {err}");
    //         return;
    //     }
    // };

    // process::Command::new("alacritty").spawn().unwrap();

    // std::thread::sleep(std::time::Duration::from_secs(3));
}
