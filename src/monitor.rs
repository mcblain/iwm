// use std::mem;
// use x11::xlib;

use crate::x::{self, DisplayRc};

// use crate::{client::Client, config, Layout};

use crate::{client::Client, Config, Layout};
use std::mem;

#[derive(Debug)]
pub struct Clients {
    pub clients: Vec<Client>,
    pub z_stack: Vec<u8>,
    pub tiling_order: Vec<u8>,
}

impl Clients {
    pub fn new() -> Self {
        Self {
            clients: Vec::new(),
            z_stack: Vec::new(),
            tiling_order: Vec::new(),
        }
    }

    // pub fn push(&mut self, mut clients: Self) {
    //     // let clients = mem::take(&mut self.clients);
    //     // let stack = mem::take(&mut self.clients_stack);
    //     // let order = mem::take(&mut self.clients_order);
    //     self.clients.append(&mut clients.clients);
    // }

    pub fn extend(&mut self, mut clients: Clients) {
        let curr_len = self.clients.len() as u8;

        for idx in &mut clients.z_stack {
            *idx += curr_len;
        }

        for idx in &mut clients.tiling_order {
            *idx += curr_len;
        }

        self.clients.extend(clients.clients);
        self.z_stack.extend(clients.z_stack);
        self.tiling_order.extend(clients.tiling_order);
    }

    pub fn take(&mut self) -> Self {
        let clients = mem::take(&mut self.clients);
        let z_stack = mem::take(&mut self.z_stack);
        let tiling_order = mem::take(&mut self.tiling_order);
        Self {
            clients,
            z_stack,
            tiling_order,
        }
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item = &'a Client> {
        self.tiling_order
            .iter()
            .map(|&idx| &self.clients[idx as usize])
    }
}

// impl Iterator for Clients {
//     type Item = Client;
//     fn next(&mut self) -> Option<Self::Item> {
//
//     }
// }

#[derive(Debug)]
pub struct Monitor {
    dpy: DisplayRc,
    pub bar_window: x::Window,
    pub bar_y: i16,
    pub clients: Clients,
    pub layout_symbol: String,
    pub lt: u8,
    pub master_area_factor: f32,
    pub mon_height: i16,
    pub mon_width: i16,
    pub mon_x: i16,
    pub mon_y: i16,
    pub nmaster: u8,
    pub num: u16,
    pub selected_client: usize,
    pub sellt: u32,
    pub seltags: usize,
    pub showbar: bool,
    pub tagset: [u32; 2],
    pub topbar: bool,
    pub window_height: i16,
    pub window_width: i16,
    pub window_x: i16,
    pub window_y: i16,
}

impl Monitor {
    pub fn new(dpy: DisplayRc, config: &Config) -> Self {
        Self {
            bar_window: 0,
            bar_y: 0,
            clients: Clients::new(),
            dpy,
            layout_symbol: String::new(),
            lt: 0,
            master_area_factor: config.master_area_factor,
            mon_height: 0,
            mon_width: 0,
            mon_x: 0,
            mon_y: 0,
            nmaster: config.master_cnt,
            num: 0,
            selected_client: 0,
            sellt: 0,
            seltags: 0,
            showbar: config.show_bar,
            tagset: [1, 1],
            topbar: config.top_bar,
            window_height: 0,
            window_width: 0,
            window_x: 0,
            window_y: 0,
        }
    }

    pub fn intersect(&self, x: i32, y: i32, w: i32, h: i32) -> i32 {
        let wx = self.window_x as i32;
        let wy = self.window_y as i32;
        let ww = self.window_width as i32;
        let wh = self.window_height as i32;
        i32::max(x + w, wx + ww - i32::max(x, wx)) * i32::max(y + h, wy + wh - i32::max(y, wy))
    }

    pub fn update_bar_pos(&mut self, bar_height: i16) {
        self.window_y = self.mon_y;
        self.window_height = self.mon_height;
        if self.showbar {
            self.window_height -= bar_height;
            if self.topbar {
                self.bar_y = self.window_y;
                self.window_y += bar_height;
            } else {
                self.bar_y = self.window_y + self.window_height;
            }
        } else {
            self.bar_y = -bar_height;
        }
    }
}

impl Drop for Monitor {
    fn drop(&mut self) {
        let mut dpy_handle = self.dpy.borrow_mut();
        x::unmap_window(&mut dpy_handle, self.bar_window);
        x::unmap_window(&mut dpy_handle, self.bar_window);
    }
}
